﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Items;
using Logic;
using Graphics;
using System.Windows.Input;
using System.Drawing;

namespace SnakeGame.Program
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<ConsoleKey, Movement> p1Movement = new Dictionary<ConsoleKey, Movement>();
            p1Movement.Add(ConsoleKey.UpArrow, Movement.Up);
            p1Movement.Add(ConsoleKey.DownArrow, Movement.Down);
            p1Movement.Add(ConsoleKey.LeftArrow, Movement.Left);
            p1Movement.Add(ConsoleKey.RightArrow, Movement.Right);

            Dictionary<ConsoleKey, Movement> p2Movement = new Dictionary<ConsoleKey, Movement>();
            p1Movement.Add(ConsoleKey.W, Movement.Up);
            p1Movement.Add(ConsoleKey.S, Movement.Down);
            p1Movement.Add(ConsoleKey.A, Movement.Left);
            p1Movement.Add(ConsoleKey.D, Movement.Right);

            IPlayerOptions p1Options = new PlayerOptions(ConsoleColor.DarkGreen, Key.Up, Key.Down, Key.Right, Key.Left);
            IPlayerOptions p2Options = new PlayerOptions(ConsoleColor.Magenta, Key.W, Key.S, Key.D, Key.A);

            Player p1 = new Player("Zsolti", p1Options, new SnakeHead(0, 0, '@', ConsoleColor.DarkGreen, Movement.Up));
            Player p2 = new Player("Eszti", p2Options, new SnakeHead(0, 0, '@', ConsoleColor.DarkCyan, Movement.Up));

            List<Player> players = new List<Player>() { p1, p2 };

            GameOptions gameOpt = new GameOptions(new Size(60, 30), 5);

            GameLogic game = new GameLogic(gameOpt, players, 100);
            Displayer displayer = new Displayer(game);
            game.Start();
        }
    }
}
