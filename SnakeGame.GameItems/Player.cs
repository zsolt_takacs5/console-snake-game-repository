﻿namespace Items
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Windows.Input;

    public class Player : IPlayer
    {
        private string name;
        private int points;
        private IPlayerOptions options;
        private SnakeHead snake;

        public string Name { get => name;}
        public int Points { get => points; set { points = value; } }
        public SnakeHead Snake { get => snake; set => snake = value; }
        public IPlayerOptions Options { get => options; set => options = value; }

        public Player(string name, IPlayerOptions playerOptions, SnakeHead snake)
        {
            this.name = name;
            this.Options = playerOptions;
            this.snake = snake;
            this.points = 0;
        }

        [DllImport("User32.dll")] //extern: Külső implementáció használata a User32.dll-ből
        public static extern short GetAsyncKeyState(int vKey);
        public void HandleMovement()
        {
            //TODO must be event driven from console key press.
            /*ConsoleKeyInfo keyInfo = Console.ReadKey(true);
            if (movementKeys.ContainsKey(keyInfo.Key))
            {
                Snake.Move(movementKeys[keyInfo.Key]);
            }
            else
            {
                Snake.Move(Movement.Idle);
            }*/
            bool keyPressed = false;
            foreach (var akt in Options.MovementKeys)
            {
                if (GetAsyncKeyState(KeyInterop.VirtualKeyFromKey(akt.Key)) != 0)
                { 
                    Snake.Move(akt.Value);
                    keyPressed = true;
                    break;
                }  //return akt.Value;
            }
            if (keyPressed == false)
            {
                //Snake.Move(Movement.Idle);
                Snake.Move(Snake.LastMovement);
            }
            //Snake.Move(Movement.Idle);
        }
        public void PickingUp(Pickup pickup, SnakeHead snake, List<Player> players)
        {
            if (snake == this.Snake && pickup.IsPickedUp == false)
            {
                Player otherPlayer = players.Where(x => x.Snake != this.Snake).FirstOrDefault();
                pickup.IsPickedUp = true;
                pickup.Behavior.Effect(this, otherPlayer);
            }
        }
    }
}
