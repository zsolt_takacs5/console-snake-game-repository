﻿namespace Items
{
    using System.Collections.Generic;

    public delegate void PickupDelegate(Pickup pickup, SnakeHead snake, List<Player> players);

    public class Pickup : GameItem
    {
        public event PickupDelegate OnPickup;

        private bool isPickedUp;

        protected IPickupBehavior behavior;
        public IPickupBehavior Behavior { get { return behavior; } set { behavior = value; } }
        public bool IsPickedUp { get { return isPickedUp; } set { isPickedUp = value; } }
        public Pickup(int x, int y, char character, IPickupBehavior behavior) : base(x, y, character, behavior.Color)
        {
            this.behavior = behavior;
            isPickedUp = false;
            //OnPickup += PickedUp;
        }
        public virtual void PickedUp(SnakeHead eater, List<Player> players)
        {
            OnPickup(this, eater, players);
        }

        public override object Clone()
        {
            return new Pickup(this.X, this.Y,this.Character, this.behavior);
        }
    }
}
