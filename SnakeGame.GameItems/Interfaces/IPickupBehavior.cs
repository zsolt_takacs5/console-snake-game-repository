﻿namespace Items
{
    using System;

    public interface IPickupBehavior
    {
        ConsoleColor Color { get; }
        int Reward { get; }
        void Effect(IPlayer playerPickuper, IPlayer otherPlayer);
    }
}