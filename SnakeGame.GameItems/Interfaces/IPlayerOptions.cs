﻿namespace Items
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Input;
    public interface IPlayerOptions
    {
        Dictionary<Key, Movement> MovementKeys { get; }
        ConsoleColor Color { get; }
    }
}
