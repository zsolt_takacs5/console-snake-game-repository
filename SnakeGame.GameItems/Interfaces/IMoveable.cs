﻿namespace Items
{
    public interface IMoveAble
    {
        int LastX { get; }
        int LastY { get; }
        int X { get; }
        int Y { get; }
        void ChangePos(int x, int y);
    }
}
