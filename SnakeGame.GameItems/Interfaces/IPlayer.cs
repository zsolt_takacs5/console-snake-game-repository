﻿namespace Items
{
    public interface IPlayer
    {
        int Points { get; set; }
        SnakeHead Snake { get; set; }
        IPlayerOptions Options { get; set; }
    }
}
