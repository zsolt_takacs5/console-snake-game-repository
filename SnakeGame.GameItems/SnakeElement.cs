﻿namespace Items
{
    using System;

    public enum Movement
    {
        Up = 0,
        Down = 1,
        Right = 2,
        Left = 3,
        Idle = 4
    }

    public class SnakeElement : GameItem, IMoveAble
    {
        private Movement lastMovement;
        private int lastX;
        private int lastY;
        public Movement LastMovement { get => lastMovement; internal protected set { lastMovement = value; } }

        public int LastX { get => lastX;}
        public int LastY { get => lastY;}

        public SnakeElement(int x, int y, char character, ConsoleColor color) : base(x, y, character, color)
        {
            lastX = x;
            lastY = y;
        }
        public SnakeElement(int x, int y, char character, ConsoleColor color, Movement lastMovement) : this(x,y,character,color)
        {
            
            this.lastMovement = lastMovement;

            switch (lastMovement)
            {
                case Movement.Up:
                    lastX = x;
                    lastY = y + 1;
                    break;
                case Movement.Down:
                    lastX = x;
                    lastY = y - 1;
                    break;
                case Movement.Right:
                    lastX = x - 1;
                    lastY = y;
                    break;
                case Movement.Left:
                    lastX = x + 1;
                    lastY = y;
                    break;
                case Movement.Idle:
                    lastX = x;
                    lastY = y;
                    break;
                default:
                    break;
            }
        }

        public override object Clone()
        {
            return new SnakeElement(this.X, this.Y, this.Character, this.Color, this.LastMovement);
        }

        public override void ChangePos(int newX, int newY)
        {
            lastX = X;
            lastY = Y;
            base.ChangePos(newX, newY);
        }
        public virtual void Move(Movement movement)
        {
            switch (movement)
            {
                case Movement.Up:
                    ChangePos(X, Y-1);
                    //Y--;
                    lastMovement = Movement.Up;
                    break;
                case Movement.Down:
                    ChangePos(X, Y+1);
                    //Y++;
                    lastMovement = Movement.Down;
                    break;
                case Movement.Right:
                    ChangePos(X+1, Y);
                    //X++;
                    lastMovement = Movement.Right;
                    break;
                case Movement.Left:
                    ChangePos(X-1, Y);
                    //X--;
                    lastMovement = Movement.Left;
                    break;
                case Movement.Idle:
                    Move(lastMovement);
                    break;

            }
        }
    }
}
