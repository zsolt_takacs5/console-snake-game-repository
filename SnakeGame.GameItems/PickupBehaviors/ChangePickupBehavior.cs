﻿namespace Items
{
    using System;

    public class ChangePickupBehavior : IPickupBehavior
    {
        private static Random rand;
        private ConsoleColor color;
        private int reward;

        public int Reward { get { return reward; } }

        static ChangePickupBehavior()
        {
            rand = new Random();
        }

        public ChangePickupBehavior()
        {
            color = ConsoleColor.Blue;
            reward = 2;
        }
        public ChangePickupBehavior(ConsoleColor color)
        {
            this.color = color;
            reward = 2;
        }

        public ChangePickupBehavior(int reward)
        {
            color = ConsoleColor.Blue;
            this.reward = reward;
        }
        public ChangePickupBehavior(ConsoleColor color, int reward)
        {
            this.color = color;
            this.reward = reward;
        }

        public ConsoleColor Color => this.color;

        public void Effect(IPlayer playerPickuper, IPlayer otherPlayer)
        {
            playerPickuper.Points += reward;
            Movement movement;
            do
            {
                movement = (Movement)rand.Next(0, Enum.GetValues(typeof(Movement)).Length); 
            } while ((!otherPlayer.Snake.CanMoveToNewDirection(movement)) && (movement == otherPlayer.Snake.LastMovement || movement == Movement.Idle));
            otherPlayer.Snake.Move(movement);
        }
    }
}
