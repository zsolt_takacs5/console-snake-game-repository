﻿namespace Items
{
    using System;
    using System.Collections.Generic;
    public class SwitchPickupBehavior : IPickupBehavior
    {
        private ConsoleColor color;
        private int reward;
        public ConsoleColor Color => this.color;
        public int Reward { get { return reward; } }

        public SwitchPickupBehavior()
        {
            color = ConsoleColor.Green;
            reward = 3;
        }

        public SwitchPickupBehavior(ConsoleColor color)
        {
            this.color = color;
            reward = 3;
        }
        public SwitchPickupBehavior(int reward)
        {
            color = ConsoleColor.Green;
            this.reward = reward;
        }

        public SwitchPickupBehavior(ConsoleColor color, int reward)
        {
            this.color = color;
            this.reward = reward;
        }

        public void Effect(List<Player> players, Player affectedPlayer)
        {
            if (players.Count > 1)
            {
                for (int i = 0; i < players.Count-1; i++)
                {
                    SnakeHead tmp = players[i].Snake;
                    players[i].Snake = players[i + 1].Snake;
                    players[i + 1].Snake = tmp;
                } 
            }
        }

        public void Effect(IPlayer playerPickuper, IPlayer otherPlayer)
        {
            SnakeHead tmp = playerPickuper.Snake;
            playerPickuper.Snake = otherPlayer.Snake;
            otherPlayer.Snake = tmp;

            playerPickuper.Snake.ChangeColor(playerPickuper.Options.Color);
            otherPlayer.Snake.ChangeColor(otherPlayer.Options.Color);

            playerPickuper.Points += reward;
        }
    }
}
