﻿namespace Items
{
    using System;
    using System.Linq;

    public class ApplePickupBehavior : IPickupBehavior
    {
        private ConsoleColor color;
        private int reward;
        public ConsoleColor Color => this.color;
        public int Reward { get { return reward; } }
        
        public ApplePickupBehavior(ConsoleColor color)
        {
            this.color = color;
            reward = 1;
        }
        public ApplePickupBehavior()
        {
            this.color = ConsoleColor.Red;
            reward = 1;
        }
        public ApplePickupBehavior(ConsoleColor color, int reward)
        {
            this.color = color;
            this.reward = reward;
        }
        public ApplePickupBehavior(int reward)
        {
            this.color = ConsoleColor.Red;
            this.reward = reward;
        }

        public void Effect(IPlayer playerPickuper, IPlayer otherPlayer)
        {
            playerPickuper.Points += reward;
            //playerPickuper.Snake.AddElement();
            //-----------------------------------------------------------------------
            SnakeElement elementBefore = null;

            if (playerPickuper.Snake.SnakeBody.Count > 0)
                elementBefore = playerPickuper.Snake.SnakeBody.Last();
            else
                elementBefore = playerPickuper.Snake;

            playerPickuper.Snake.SnakeBody.Add(
                    new SnakeElement(elementBefore.LastX,
                    elementBefore.LastY,
                    elementBefore.Character,
                    playerPickuper.Options.Color,
                    elementBefore.LastMovement));

            playerPickuper.Snake.AddElement(); // call the event
        }

    }
}
