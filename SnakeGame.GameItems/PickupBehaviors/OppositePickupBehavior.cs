﻿namespace Items
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class OppositePickupBehavior : IPickupBehavior
    {
        private ConsoleColor color;
        private int reward;
        public ConsoleColor Color => this.color;
        public int Reward { get { return reward; } }

        public OppositePickupBehavior()
        {
            color = ConsoleColor.Yellow;
            reward = 3;
        }

        public OppositePickupBehavior(ConsoleColor color)
        {
            this.color = color;
            reward = 3;
        }

        public OppositePickupBehavior(int reward)
        {
            color = ConsoleColor.Yellow;
            this.reward = reward;
        }

        public OppositePickupBehavior(ConsoleColor color, int reward)
        {
            this.color = color;
            this.reward = reward;
        }

        public void Effect(List<Player> players, Player affectedPlayer)
        {
            foreach (var player in players)
            {
                int tmpFirstX = player.Snake.X;
                int tmpFirstY = player.Snake.Y;

                player.Snake.X = player.Snake.SnakeBody.Last().X;
                player.Snake.Y = player.Snake.SnakeBody.Last().Y;

                player.Snake.SnakeBody.Last().X = tmpFirstX;
                player.Snake.SnakeBody.Last().Y = tmpFirstY;
            }
        }

        public void Effect(IPlayer playerPickuper, IPlayer otherPlayer)
        {
            playerPickuper.Points += reward;
            List<IPlayer> players = new List<IPlayer>() { playerPickuper, otherPlayer };
            foreach (var player in players)
            {

                int tmpFirstX = player.Snake.X;
                int tmpFirstY = player.Snake.Y;

                if (player.Snake.SnakeBody.Count > 0)
                {

                    player.Snake.X = player.Snake.SnakeBody.Last().X;
                    player.Snake.Y = player.Snake.SnakeBody.Last().Y;

                    player.Snake.SnakeBody.Last().X = tmpFirstX;
                    player.Snake.SnakeBody.Last().Y = tmpFirstY; 
                }
                player.Snake.LastMovement = player.Snake.OppositeMovement(player.Snake.LastMovement);
                //player.Snake.Move(player.Snake.OppositeMovement(player.Snake.LastMovement));
            }
        }
    }
}
