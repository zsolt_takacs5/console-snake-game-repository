﻿namespace Logic
{
    using System.Drawing;

    public class GameOptions
    {
        private Size size;
        private int volumeLevel;

        public Size Size { get => size;}
        public int VolumeLevel { get => volumeLevel; set => volumeLevel = value; }

        public GameOptions(Size size, int volumeLevel)
        {
            this.size = size;
            this.volumeLevel = volumeLevel;
        }
    }
}
