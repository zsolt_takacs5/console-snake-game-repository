﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SnakeMultiplayerGame.Items
{
    public class Options
    {
        private Dictionary<Key, Movement> movementKeys;
        private ConsoleColor color;

        public Dictionary<Key, Movement> MovementKeys { get => movementKeys;}
        public ConsoleColor Color { get => color;}

        public Options(ConsoleColor color, Key up, Key down, Key right, Key left)
        {
            this.color = color;
            movementKeys = new Dictionary<Key, Movement>();
            movementKeys.Add(up, Movement.Up);
            movementKeys.Add(down, Movement.Down);
            movementKeys.Add(right, Movement.Right);
            movementKeys.Add(left, Movement.Left);
            movementKeys.Add(Key.None, Movement.Idle);
        }
    }
}
