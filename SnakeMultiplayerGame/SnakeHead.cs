﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeMultiplayerGame.Items
{
    public class SnakeHead : SnakeElement
    {
        private List<SnakeElement> snakeBody;
        //private Movement lastMovement;
        //public Movement LastMovement { get => lastMovement;}
        public List<SnakeElement> SnakeBody { get { return snakeBody; } }

        public delegate void NewSnakeElementDelegate(SnakeElement element);
        public event NewSnakeElementDelegate OnElementAdded;

        public SnakeHead(int x, int y, char character, ConsoleColor color, Movement startMovement) : base(x, y, character, color)
        {
            snakeBody = new List<SnakeElement>();
            LastMovement = startMovement;
        }


        public override void Move(Movement movement)
        {
            /*TODO 
             * last movement up - cant go down
             * last movement down - cant go up
             * last movement right - cant go left
             * last movement left - cant go right
             */

            /*
            if (movement != LastMovement)
            {
                base.Move(movement);
            }
            else
            {
                base.Move(Movement.Idle);
            }
            */
            if (movement != Movement.Idle && CanMoveToNewDirection(movement))
            {
                base.Move(movement);
                /*
                foreach (var element in SnakeBody)
                {
                    element.Move(LastMovement);
                }*/
                for (int i = 0; i < SnakeBody.Count; i++)
                {
                    if (i == 0)
                    {
                        SnakeBody[i].ChangePos(this.LastX,this.LastY);
                    }
                    else
                    {
                        SnakeBody[i].ChangePos(SnakeBody[i - 1].LastX, SnakeBody[i - 1].LastY);
                    }
                }
            }
            else
            {
                base.Move(LastMovement);
                //foreach (var element in SnakeBody)
                //{
                //    element.Move(Movement.Idle);
                //}
                for (int i = 0; i < SnakeBody.Count; i++)
                {
                    if (i == 0)
                    {
                        SnakeBody[i].ChangePos(this.LastX, this.LastY);
                    }
                    else
                    {
                        SnakeBody[i].ChangePos(SnakeBody[i - 1].LastX, SnakeBody[i - 1].LastY);
                    }
                }
            }
        }
        public void ChangePos(Movement movement)
        {
            switch (movement)
            {
                case Movement.Up:
                    ChangePos(this.X, this.Y-1);
                    break;
                case Movement.Down:
                    ChangePos(this.X, this.Y+1);
                    break;
                case Movement.Right:
                    ChangePos(this.X+1, this.Y);
                    break;
                case Movement.Left:
                    ChangePos(this.X-1, this.Y);
                    break;
                case Movement.Idle:
                    ChangePos(this.LastMovement);
                    break;
                default:
                    ChangePos(this.LastMovement);
                    break;
            }
        }
        public override void ChangePos(int newX, int newY)
        {
            base.ChangePos(newX, newY);
            //for (int i = 0; i < SnakeBody.Count; i++)
            //{
            //    if (i == 0)
            //    {
            //        SnakeBody[0].ChangePos(this.LastX, this.LastY);
            //    }
            //    else
            //    {
            //        SnakeBody[i].ChangePos(SnakeBody[i - 1].LastX,
            //            SnakeBody[i - 1].LastY);
            //    }
            //}
        }
        internal bool CanMoveToNewDirection(Movement newMovement)
        {
            switch (LastMovement)
            {
                case Movement.Up:
                    if (newMovement == Movement.Down)
                    {
                        return false;
                    }
                    break;
                case Movement.Down:
                    if (newMovement == Movement.Up)
                    {
                        return false;
                    }
                    break;
                case Movement.Right:
                    if (newMovement == Movement.Left)
                    {
                        return false;
                    }
                    break;
                case Movement.Left:
                    if (newMovement == Movement.Right)
                    {
                        return false;
                    }
                    break;
                case Movement.Idle:
                    return true;
            }
            return true;
        }
        public void AddElement()
        {
            /*
            if (SnakeBody.Count > 0)
            {
                snakeBody.Add(new SnakeElement(SnakeBody.Last().LastX, SnakeBody.Last().LastY, 
                    SnakeBody.Last().Character, SnakeBody.Last().Color, SnakeBody.Last().LastMovement));
            }
            else
            {
                snakeBody.Add(new SnakeElement(this.LastX, this.LastY, this.Character, this.Color, this.LastMovement));
            }
            */
            //snakeBody.Insert(0, new SnakeElement(x, y, 'o', ConsoleColor.White));

            //snakeBody.Insert(0, new SnakeElement(x, y, base.Character, base.Color));
            //ChangePos(this.LastMovement);

            OnElementAdded?.Invoke(SnakeBody.Last());
            //snakeBody.Add(new SnakeElement(x, y, base.Character, base.Color));
        }
        public override object Clone()
        {
            SnakeHead clone = new SnakeHead(this.X, this.Y, this.Character, this.Color, this.LastMovement);
            foreach (var item in snakeBody)
            {
                clone.snakeBody.Add(item.Clone() as SnakeElement);
            }
            return clone;
        }
        public override void ChangeColor(ConsoleColor newColor)
        {
            base.ChangeColor(newColor);
            foreach (var element in SnakeBody)
            {
                element.ChangeColor(newColor);
            }
        }

        public Movement OppositeMovement(Movement movement)
        {
            switch (movement)
            {
                case Movement.Up:
                    return Movement.Down;
                case Movement.Down:
                    return Movement.Up;
                case Movement.Right:
                    return Movement.Left;
                case Movement.Left:
                    return Movement.Right;
                case Movement.Idle:
                    return OppositeMovement(LastMovement);
                default:
                    return OppositeMovement(LastMovement);
            }
        }
    }
}
