﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeMultiplayerGame.Items
{
    public interface IMoveAble
    {
        int LastX { get;}
        int LastY { get;}
        int X { get;}
        int Y { get;}
        void ChangePos(int x, int y);
    }
    public enum Movement
    {
        Up = 0,
        Down = 1,
        Right = 2,
        Left = 3,
        Idle = 4
    }
    public abstract class GameItem : ICloneable
    {
        private int x;
        private int y;
        private ConsoleColor color;
        private char character;

        public int X { get => x; protected internal set => x = value; }
        public int Y { get => y; protected internal set => y = value; }
        public ConsoleColor Color { get => color; protected set => color = value; }
        public char Character { get => character; protected set => character = value; }

        protected GameItem(int x, int y, char character, ConsoleColor color)
        {
            X = x;
            Y = y;
            Character = character;
            Color = color;
        }

        public override string ToString()
        {
            return Character.ToString();
        }
        public virtual void ChangeColor(ConsoleColor newColor)
        {
            color = newColor;
        }
        public virtual void ChangePos(int newX, int newY)
        {
            x = newX;
            y = newY;
        }

        public abstract object Clone();
    }
}
