﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SnakeMultiplayerGame.Items;

namespace SnakeMultiplayerGame.Logic
{
    public class GameLogic
    {

        private Random rand;
        private List<GameItem> items;
        private List<Player> players;
        private List<Pickup> pickupsToSpawn;
        private bool isGameOver;
        private GameOptions gameOptions;
        private object lockObject;
        private int tickTime;

        public delegate void TickDelegate();
        public delegate void ItemDelegate(GameItem item);
        public event TickDelegate OnTicked;
        public event ItemDelegate OnItemRemoved;
        public GameLogic(GameOptions gameOptions, List<Player> players, int millisecondsTick)
        {
            lockObject = new object();
            rand = new Random();
            isGameOver = false;
            this.gameOptions = gameOptions;
            Players = players;
            pickupsToSpawn = new List<Pickup>();
            tickTime = millisecondsTick;

            pickupsToSpawn.Add(new Pickup(0, 0, '×', new ApplePickupBehavior()));
            pickupsToSpawn.Add(new Pickup(0, 0, '×', new ChangePickupBehavior()));
            pickupsToSpawn.Add(new Pickup(0, 0, '×', new SwitchPickupBehavior()));
            pickupsToSpawn.Add(new Pickup(0, 0, '×', new OppositePickupBehavior()));

            items = new List<GameItem>();

            foreach (var player in this.players)
            {
                items.Add(player.Snake);
                player.Snake.OnElementAdded += Snake_OnElementAdded;
            }
            /*
            Console.WindowLeft = width;
            Console.WindowTop = height;
            */
        }

        private void Snake_OnElementAdded(SnakeElement element)
        {
            items.Add(element);
        }

        public List<GameItem> Items
        {
            get
            {
                
                List<GameItem> clone = new List<GameItem>();
                foreach (var item in items)
                {
                    clone.Add(item);
                };
                
                return clone;
            }
            //set => items = value;
        }

        public List<Player> Players { get => players; set => players = value; }
        public GameOptions Options { get => gameOptions; }
        
        public void SubscribeToPickup(Pickup pickup, Player player)
        {
            pickup.OnPickup += player.PickingUp;
        }
        private void BoundaryCheck(SnakeElement item)
        {
            if (item.X < 0)
            {
                int newX = (int)gameOptions.Size.Width;
                item.ChangePos(newX, item.Y);
            }
            if (item.X > gameOptions.Size.Width)
            {
                //item.X = 0;
                item.ChangePos(0, item.Y);
            }
            if (item.Y < 0)
            {
                int newY = (int)gameOptions.Size.Height;
                item.ChangePos(item.X, newY);
            }
            if (item.Y > gameOptions.Size.Height)
            {
                //item.Y = 0;
                item.ChangePos(item.X, 0);
            }
        }
        public void CollisionCheck()
        {
            foreach (var player in Players)
            {
                foreach (var itemToCompare in Items)
                {
                    if (player.Snake != itemToCompare)
                    {
                        if (player.Snake.X == itemToCompare.X && player.Snake.Y == itemToCompare.Y)
                        {
                            if (itemToCompare is Pickup pickup)
                            {
                                pickup.PickedUp(player.Snake, Players);
                                //itemsToRemove.Add(pickup);
                                items.Remove(pickup);
                                OnItemRemoved(pickup);
                                //PickupSpawn(new ChangePickupBehavior());
                                //PickupSpawn(new ApplePickupBehavior());
                                //PickupSpawn(new SwitchPickupBehavior());
                                PickupSpawn(new OppositePickupBehavior());
                                //PickupSpawn();
                            }
                            else if (itemToCompare is SnakeElement snake)
                            {
                                isGameOver = true;
                                GameOver(players.Where(x => x.Name != player.Name).FirstOrDefault());
                                return;
                            }
                            //else
                            //{
                            //    isGameOver = true;
                            //    Player winner = null;
                            //    int maxPoint = 0;
                            //    foreach (var p in players)
                            //    {
                            //        if (p.Points > maxPoint)
                            //        {
                            //            maxPoint = p.Points;
                            //            winner = p;
                            //        }
                            //    }
                            //    GameOver(winner);
                            //}
                        }
                    }
                }
                
                BoundaryCheck(player.Snake);
                foreach (var element in player.Snake.SnakeBody)
                {
                    BoundaryCheck(element);
                }
            }
        }
        private bool IsFreeSpace(GameItem item)
        {
            foreach (var gameItem in items)
            {
                if (gameItem != item && gameItem.X == item.X && gameItem.Y == item.Y)
                {
                    return false;
                }
            }
            return true;
        }
        private bool IsFreeSpace(int x, int y)
        {
            foreach (var gameItem in items)
            {
                if (gameItem.X == x && gameItem.Y == y)
                {
                    return false;
                }
            }
            return true;
        }
        private void PickupSpawn()
        {
            Pickup newPickup = pickupsToSpawn[rand.Next(0, pickupsToSpawn.Count)].Clone() as Pickup;
            //comment out this
            //newPickup.Behavior = behavior;
            //
            int xCoord = 0;
            int yCoord = 0;
            do
            {
                xCoord = rand.Next(0, (int)gameOptions.Size.Width);
                yCoord = rand.Next(0, (int)gameOptions.Size.Height);
            } while (!IsFreeSpace(xCoord, yCoord));
            newPickup.X = xCoord;
            newPickup.Y = yCoord;
            items.Add(newPickup);
            foreach (var player in Players)
            {
                SubscribeToPickup(newPickup, player);
            }
        }
        private void PickupSpawn(IPickupBehavior behavior)
        {
            Pickup newPickup = pickupsToSpawn[rand.Next(0, pickupsToSpawn.Count)].Clone() as Pickup;
            //comment out this
            newPickup.Behavior = behavior;
            newPickup.ChangeColor(behavior.Color);
            //
            int xCoord = 0;
            int yCoord = 0;
            do
            {
                xCoord = rand.Next(0,(int)gameOptions.Size.Width);
                yCoord = rand.Next(0, (int)gameOptions.Size.Height);
            } while (!IsFreeSpace(xCoord,yCoord));
            newPickup.X = xCoord;
            newPickup.Y = yCoord;
            items.Add(newPickup);
            foreach (var player in Players)
            {
                SubscribeToPickup(newPickup, player);
            }
        }
        private void GameOver(Player winner)
        {
            Console.WriteLine("GAME OVER!");
            Console.WriteLine($"Winner is: {winner.Name}, with {winner.Points}");
            Console.ReadLine();
        }
        private void Tick()
        {
            Thread.Sleep(tickTime);
            foreach (var player in players)
            {
                player.HandleMovement();
            }
            CollisionCheck();
        }
        public void Start()
        {
            foreach (var player in players)
            {
                player.Snake.X = rand.Next(0, (int)gameOptions.Size.Width);
                player.Snake.Y = rand.Next(0, (int)gameOptions.Size.Height);
                player.Snake.Move((Movement)rand.Next(0, 4));
            }
            //DrawBorder();
            //PickupSpawn(new OppositePickupBehavior());
            //PickupSpawn(new SwitchPickupBehavior());
            PickupSpawn(new ApplePickupBehavior());
            while (!isGameOver)
            {
                //Console.Clear();
                Tick();
                //OnTicked?.Invoke(Items);
                OnTicked();


                //foreach (var item in items)
                //{
                //    Console.WriteLine($"{item.Character} - X:{item.X} Y:{item.Y}");
                //}
                //Console.WriteLine("-----------------------------------------------------");
                //Display();
            }
            GameOver(players.OrderByDescending(x=>x.Points).FirstOrDefault());
        }
    }
}
