﻿using System;
using System.Collections.Generic;

namespace SnakeMultiplayerGame.Items
{
    public interface IPickupBehavior
    {
        ConsoleColor Color { get; }
        int Reward { get; }
        void Effect(IPlayer playerPickuper, IPlayer otherPlayer);
    }
}