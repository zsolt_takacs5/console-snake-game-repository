﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeMultiplayerGame.Items
{
    public interface IPlayer
    {
        int Points { get; set; }
        SnakeHead Snake { get; set; }
        Options Options { get; set; }
    }
}
