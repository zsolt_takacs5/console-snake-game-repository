﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SnakeMultiplayerGame.Logic
{
    public class GameOptions
    {
        private Size size;
        private int volumeLevel;

        public Size Size { get => size;}
        public int VolumeLevel { get => volumeLevel; set => volumeLevel = value; }

        public GameOptions(Size size, int volumeLevel)
        {
            this.size = size;
            this.volumeLevel = volumeLevel;
        }
    }
}
