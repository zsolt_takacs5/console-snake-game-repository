﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnakeMultiplayerGame.Logic;
using SnakeMultiplayerGame.Items;

namespace SnakeMultiplayerGame.Graphics
{
    public class Displayer
    {
        private GameLogic logic;
        public Displayer(GameLogic logic)
        {
            this.logic = logic;
            this.logic.OnTicked += Logic_OnTicked;
            this.logic.OnItemRemoved += RemoveItem;
            Console.CursorVisible = true;
        }
        private void DrawPoints(int height)
        {
            foreach (var player in logic.Players)
            {
                    Console.SetCursorPosition(5, height+logic.Players.IndexOf(player));
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"{player.Name} : {player.Points}");
                    Console.ResetColor();
            }
        }
        private void Logic_OnTicked()
        {
            //RedrawElements_New();
            DrawBorder("#");
            foreach (var item in logic.Items)
            {
                RedrawElement(item);
            }
            DrawPoints((int)logic.Options.Size.Height+5);
        }

        //private void ClearFromScreen(GameItem item)
        //{
        //    Console.SetCursorPosition(item.X, item.Y);
        //    Console.Write("");
        //}
        private bool OutOfBorder(int x, int y)
        {
            bool eredmeny = x < 0 || x > logic.Options.Size.Width || y < 0 || y > logic.Options.Size.Height;
            return eredmeny;
        }
        private void RemoveItem(GameItem item)
        {
            Console.SetCursorPosition(item.X, item.Y);
            Console.Write(" ");
        }
        private bool SameCoordinatesAsOld(int newX, int oldX, int newY, int oldY)
        {
            bool eredmeny = newX == oldX && newY == oldY;
            return eredmeny;
        }
        public void RedrawElements_New()
        {
            //draw static elements
            foreach (var item in logic.Items)
            {
                if (!(item is IMoveAble))
                {
                    Console.SetCursorPosition(item.X, item.Y);
                    Console.ForegroundColor = item.Color;
                    Console.Write(item.Character);
                    Console.ResetColor();
                }
            }
            //clear moveable element's last coordinate
            foreach (var item in logic.Items)
            {
                if (item is IMoveAble moveable)
                {
                    Console.SetCursorPosition(item.X, item.Y);
                    Console.Write(" ");
                    if (!OutOfBorder(moveable.LastX,moveable.LastY))
                    {
                        Console.SetCursorPosition(moveable.LastX, moveable.LastY);
                        Console.Write(" "); 
                    }
                }
            }

            //draw moveable element's current coordinate
            foreach (var item in logic.Items)
            {
                if (item is IMoveAble moveable)
                {
                    Console.SetCursorPosition(item.X, item.Y);
                    Console.ForegroundColor = item.Color;
                    Console.Write(item.Character);
                    Console.ResetColor();
                }
            }
        }
        public void RedrawElement(GameItem item)
        {
            

            /*
             * Draw the item in the current Coordinates
             * If the item is moveable: clear the old position of the item.
             */
            Console.SetCursorPosition(item.X, item.Y);
            //if (item is IMoveAble)
            //{
            //    Console.BackgroundColor = ConsoleColor.Cyan;
            //}
            Console.ForegroundColor = item.Color;
            Console.Write(item.ToString());
            Console.ResetColor();

            if (item is IMoveAble moveItem)
            {

                //Console.SetCursorPosition(0, (int)logic.Options.Size.Height + 1);
                //Console.Write($"OldX: {moveItem.LastX} OldY: {moveItem.LastY} | Current X: {moveItem.X} Current Y: {moveItem.Y}");

                if ((!OutOfBorder(moveItem.LastX, moveItem.LastY)) && !SameCoordinatesAsOld(moveItem.X, moveItem.LastX, moveItem.Y, moveItem.LastY))//(moveItem.LastX != moveItem.X && moveItem.LastY != moveItem.Y))
                {
                    Console.SetCursorPosition(moveItem.LastX, moveItem.LastY);
                    Console.Write(" ");
                }
            }



        }
        private void DrawBorder(string borderStyle)
        {
            for (int i = 0; i < (int)logic.Options.Size.Width + 1; i++)
            {
                //draw horizontal + vertical lines
                if (i == 0 || i == (int)logic.Options.Size.Width)
                {
                    for (int j = 0; j < logic.Options.Size.Height; j++)
                    {
                        Console.SetCursorPosition(i, j);
                        Console.Write(borderStyle);
                    }
                }
                //draw horizontal lines
                else
                {
                    Console.SetCursorPosition(i, 0);
                    Console.Write(borderStyle);
                    Console.SetCursorPosition(i, (int)logic.Options.Size.Height);
                    Console.Write(borderStyle);
                }
            }
        }
        //public void Draw()
        //{
        //    foreach (var item in logic.Items)
        //    {
        //        RedrawElement(item);
        //    }
        //}
        //public void Display()
        //{
        //    //Console.Clear();
        //    DrawBorder("#");
        //    foreach (var item in logic.Items)
        //    {
        //        Console.SetCursorPosition(item.X, item.Y);
        //        Console.ForegroundColor = item.Color;
        //        Console.Write(item.ToString());
        //        Console.ResetColor();
        //    }
        //    //Tick();
        //}
    }
}
