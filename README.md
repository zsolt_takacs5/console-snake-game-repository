# README #

* Multiplayer snake game made for fun. 

### Usage ###

* UP,RIGHT,LEFT,DOWN arrows - player 1
* W,A,S,D - player 2

### Pickups ###

* There are multiple type pickups in the game. Each of them has a different color.
* Yellow: Make the snakes reversing their movement by replacing their head with their tail.
* Red: Adds 1 element at the end of the snake.
* Blue: Changes the direction of the opposite snake.
* Green: Switching up the controls of the snakes. (player1 controls snake2, player2 controls snake1)
